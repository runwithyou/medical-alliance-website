from flasgger import Swagger
from flask import Flask, render_template, request
from flask_cors import CORS
import json

app = Flask(__name__, template_folder='templates', static_folder='static', static_url_path='/static')
app.config['JSON_AS_ASCII'] = False
app.config['JSONIFY_MIMETYPE'] = "application/json;charset=utf-8"
CORS(app, suppors_credentials=True, resources={r'/*'})  # 设置跨域
swagger = Swagger(app)


@app.route('/')
def getdataHtml():
    return render_template('echart.html')


@app.route('/view', methods=['GET'])
def getview():
    # global type_n_g, name_g
    data = {}
    data['linksJson'] = [{
        "name": "根节点1",
        "value": 0,
        "list": [{
            "name": "节点1",
            "value": 1,
            "list": [{
                "name": "节点1-1-1",
                "value": "1-1-1",
                "list": [{
                    "name": "节点1-1-1-1",
                    "value": "1-1-1-1"
                }, {
                    "name": "节点1-1-1-2",
                    "value": "1-1-1-2"
                }, {
                    "name": "节点1-1-1-3",
                    "value": "1-1-1-3"
                }]
            }, {
                "name": "节点1-1-2",
                "value": "1-1-2"
            }, {
                "name": "节点1-1-3",
                "value": "1-1-3",
                "list": [{
                    "name": "节点1-1-3-1",
                    "value": "1-1-3-1"
                }, {
                    "name": "节点1-1-3-2",
                    "value": "1-1-3-2"
                }, {
                    "name": "节点1-1-3-3",
                    "value": "1-1-3-3"
                }]
            }, {
                "name": "节点1-1-4",
                "value": "1-1-4"
            }, {
                "name": "节点1-1-5",
                "value": "1-1-5",
                "list": [{
                    "name": "节点1-1-5-1",
                    "value": "1-1-5-1"
                }, {
                    "name": "节点1-1-5-2",
                    "value": "1-1-5-2"
                }, {
                    "name": "节点1-1-5-3",
                    "value": "1-1-5-3"
                }]
            }]
        }, {
            "name": "节点2",
            "value": 2,
            "list": [{
                "name": "节点1-2-1",
                "value": "1-2-1",
                "list": [{
                    "name": "节点1-2-1-1",
                    "value": "1-2-1-1"
                }, {
                    "name": "节点1-2-1-2",
                    "value": "1-2-1-2"
                }, {
                    "name": "节点1-2-1-3",
                    "value": "1-2-1-3"
                }]
            }, {
                "name": "节点1-2-2",
                "value": "1-2-2"
            }, {
                "name": "节点1-2-3",
                "value": "1-2-3",
                "list": [{
                    "name": "节点1-2-3-1",
                    "value": "1-2-3-1"
                }, {
                    "name": "节点1-2-3-2",
                    "value": "1-2-3-2"
                }, {
                    "name": "节点1-2-3-3",
                    "value": "1-2-3-3"
                }]
            }, {
                "name": "节点1-2-4",
                "value": "1-2-4"
            }, {
                "name": "节点1-2-5",
                "value": "1-2-5",
                "list": [{
                    "name": "节点1-2-5-1",
                    "value": "1-2-5-1"
                }, {
                    "name": "节点1-2-5-2",
                    "value": "1-2-5-2"
                }, {
                    "name": "节点1-2-5-3",
                    "value": "1-2-5-3"
                }]
            }]
        }, {
            "name": "节点3",
            "value": 3,
            "list": [{
                "name": "节点1-3-1",
                "value": "1-3-1",
                "list": [{
                    "name": "节点1-3-1-1",
                    "value": "1-3-1-1"
                }, {
                    "name": "节点1-3-1-2",
                    "value": "1-3-1-2"
                }, {
                    "name": "节点1-3-1-3",
                    "value": "1-3-1-3"
                }]
            }, {
                "name": "节点1-3-2",
                "value": "1-3-2"
            }, {
                "name": "节点1-3-3",
                "value": "1-3-3",
                "list": [{
                    "name": "节点1-3-3-1",
                    "value": "1-3-3-1"
                }, {
                    "name": "节点1-3-3-2",
                    "value": "1-3-3-2"
                }, {
                    "name": "节点1-3-3-3",
                    "value": "1-3-3-3"
                }]
            }, {
                "name": "节点1-3-4",
                "value": "1-3-4"
            }, {
                "name": "节点1-3-5",
                "value": "1-3-5",
                "list": [{
                    "name": "节点1-3-5-1",
                    "value": "1-3-5-1"
                }, {
                    "name": "节点1-3-5-2",
                    "value": "1-3-5-2"
                }, {
                    "name": "节点1-3-5-3",
                    "value": "1-3-5-3"
                }]
            }]
        }, {
            "name": "节点4",
            "value": 4,
            "list": [{
                "name": "节点1-4-1",
                "value": "1-4-1",
                "list": [{
                    "name": "节点1-4-1-1",
                    "value": "1-4-1-1"
                }, {
                    "name": "节点1-4-1-2",
                    "value": "1-4-1-2"
                }, {
                    "name": "节点1-4-1-3",
                    "value": "1-4-1-3"
                }]
            }, {
                "name": "节点1-4-2",
                "value": "1-4-2"
            }, {
                "name": "节点1-4-3",
                "value": "1-4-3",
                "list": [{
                    "name": "节点1-4-3-1",
                    "value": "1-4-3-1"
                }, {
                    "name": "节点1-4-3-2",
                    "value": "1-4-3-2"
                }, {
                    "name": "节点1-4-3-3",
                    "value": "1-4-3-3"
                }]
            }, {
                "name": "节点1-4-4",
                "value": "1-4-4"
            }, {
                "name": "节点1-4-5",
                "value": "1-4-5",
                "list": [{
                    "name": "节点1-4-5-1",
                    "value": "1-4-5-1"
                }, {
                    "name": "节点1-4-5-2",
                    "value": "1-4-5-2"
                }, {
                    "name": "节点1-4-5-3",
                    "value": "1-4-5-3"
                }]
            }]
        }, {
            "name": "节点5",
            "value": 5,
            "list": [{
                "name": "节点1-5-1",
                "value": "1-5-1",
                "list": [{
                    "name": "节点1-5-1-1",
                    "value": "1-5-1-1"
                }, {
                    "name": "节点1-5-1-2",
                    "value": "1-5-1-2"
                }, {
                    "name": "节点1-5-1-3",
                    "value": "1-5-1-3"
                }]
            }, {
                "name": "节点1-5-2",
                "value": "1-5-2"
            }, {
                "name": "节点1-5-3",
                "value": "1-5-3",
                "list": [{
                    "name": "节点1-5-3-1",
                    "value": "1-5-3-1"
                }, {
                    "name": "节点1-5-3-2",
                    "value": "1-5-3-2"
                }, {
                    "name": "节点1-5-3-3",
                    "value": "1-5-3-3"
                }]
            }, {
                "name": "节点1-5-4",
                "value": "1-5-4"
            }, {
                "name": "节点1-5-5",
                "value": "1-5-5",
                "list": [{
                    "name": "节点1-5-5-1",
                    "value": "1-5-5-1"
                }, {
                    "name": "节点1-5-5-2",
                    "value": "1-5-5-2"
                }, {
                    "name": "节点1-5-5-3",
                    "value": "1-5-5-3"
                }]
            }]
        }, {
            "name": "节点6",
            "value": 6,
            "list": [{
                "name": "节点1-6-1",
                "value": "1-6-1",
                "list": [{
                    "name": "节点1-6-1-1",
                    "value": "1-6-1-1"
                }, {
                    "name": "节点1-6-1-2",
                    "value": "1-6-1-2"
                }, {
                    "name": "节点1-6-1-3",
                    "value": "1-6-1-3"
                }]
            }, {
                "name": "节点1-6-2",
                "value": "1-6-2"
            }, {
                "name": "节点1-6-3",
                "value": "1-6-3",
                "list": [{
                    "name": "节点1-6-3-1",
                    "value": "1-6-3-1"
                }, {
                    "name": "节点1-6-3-2",
                    "value": "1-6-3-2"
                }, {
                    "name": "节点1-6-3-3",
                    "value": "1-6-3-3"
                }]
            }, {
                "name": "节点1-6-4",
                "value": "1-6-4"
            }, {
                "name": "节点1-6-5",
                "value": "1-6-5",
                "list": [{
                    "name": "节点1-6-5-1",
                    "value": "1-6-5-1"
                }, {
                    "name": "节点1-6-5-2",
                    "value": "1-6-5-2"
                }, {
                    "name": "节点1-6-5-3",
                    "value": "1-6-5-3"
                }]
            }]
        }, {
            "name": "节点7",
            "value": 7,
            "list": [{
                "name": "节点1-7-1",
                "value": "1-7-1",
                "list": [{
                    "name": "节点1-7-1-1",
                    "value": "1-7-1-1"
                }, {
                    "name": "节点1-7-1-2",
                    "value": "1-7-1-2"
                }, {
                    "name": "节点1-7-1-3",
                    "value": "1-7-1-3"
                }]
            }, {
                "name": "节点1-7-2",
                "value": "1-7-2"
            }, {
                "name": "节点1-7-3",
                "value": "1-7-3",
                "list": [{
                    "name": "节点1-7-3-1",
                    "value": "1-7-3-1"
                }, {
                    "name": "节点1-7-3-2",
                    "value": "1-7-3-2"
                }, {
                    "name": "节点1-7-3-3",
                    "value": "1-7-3-3"
                }]
            }, {
                "name": "节点1-7-4",
                "value": "1-7-4"
            }, {
                "name": "节点1-7-5",
                "value": "1-7-5",
                "list": [{
                    "name": "节点1-7-5-1",
                    "value": "1-7-5-1"
                }, {
                    "name": "节点1-7-5-2",
                    "value": "1-7-5-2"
                }, {
                    "name": "节点1-7-5-3",
                    "value": "1-7-5-3"
                }]
            }]
        }, {
            "name": "节点8",
            "value": 8,
            "list": [{
                "name": "节点1-8-1",
                "value": "1-8-1",
                "list": [{
                    "name": "节点1-8-1-1",
                    "value": "1-8-1-1"
                }, {
                    "name": "节点1-8-1-2",
                    "value": "1-8-1-2"
                }, {
                    "name": "节点1-8-1-3",
                    "value": "1-8-1-3"
                }]
            }, {
                "name": "节点1-8-2",
                "value": "1-8-2"
            }, {
                "name": "节点1-8-3",
                "value": "1-8-3",
                "list": [{
                    "name": "节点1-8-3-1",
                    "value": "1-8-3-1"
                }, {
                    "name": "节点1-8-3-2",
                    "value": "1-8-3-2"
                }, {
                    "name": "节点1-8-3-3",
                    "value": "1-8-3-3"
                }]
            }, {
                "name": "节点1-8-4",
                "value": "1-8-4"
            }, {
                "name": "节点1-8-5",
                "value": "1-8-5",
                "list": [{
                    "name": "节点1-8-5-1",
                    "value": "1-8-5-1"
                }, {
                    "name": "节点1-8-5-2",
                    "value": "1-8-5-2"
                }, {
                    "name": "节点1-8-5-3",
                    "value": "1-8-5-3"
                }]
            }]
        }, {
            "name": "节点9",
            "value": 9,
            "list": [{
                "name": "节点1-9-1",
                "value": "1-9-1",
                "list": [{
                    "name": "节点1-9-1-1",
                    "value": "1-9-1-1"
                }, {
                    "name": "节点1-9-1-2",
                    "value": "1-9-1-2"
                }, {
                    "name": "节点1-9-1-3",
                    "value": "1-9-1-3"
                }]
            }, {
                "name": "节点1-9-2",
                "value": "1-9-2"
            }, {
                "name": "节点1-9-3",
                "value": "1-9-3",
                "list": [{
                    "name": "节点1-9-3-1",
                    "value": "1-9-3-1"
                }, {
                    "name": "节点1-9-3-2",
                    "value": "1-9-3-2"
                }, {
                    "name": "节点1-9-3-3",
                    "value": "1-9-3-3"
                }]
            }, {
                "name": "节点1-9-4",
                "value": "1-9-4"
            }, {
                "name": "节点1-9-5",
                "value": "1-9-5",
                "list": [{
                    "name": "节点1-9-5-1",
                    "value": "1-9-5-1"
                }, {
                    "name": "节点1-9-5-2",
                    "value": "1-9-5-2"
                }, {
                    "name": "节点1-9-5-3",
                    "value": "1-9-5-3"
                }]
            }]
        }, {
            "name": "节点10",
            "value": 10,
            "list": [{
                "name": "节点1-10-1",
                "value": "1-10-1",
                "list": [{
                    "name": "节点1-10-1-1",
                    "value": "1-10-1-1"
                }, {
                    "name": "节点1-10-1-2",
                    "value": "1-10-1-2"
                }, {
                    "name": "节点1-10-1-3",
                    "value": "1-10-1-3"
                }]
            }, {
                "name": "节点1-10-2",
                "value": "1-10-2"
            }, {
                "name": "节点1-10-3",
                "value": "1-10-3",
                "list": [{
                    "name": "节点1-10-3-1",
                    "value": "1-10-3-1"
                }, {
                    "name": "节点1-10-3-2",
                    "value": "1-10-3-2"
                }, {
                    "name": "节点1-10-3-3",
                    "value": "1-10-3-3"
                }]
            }, {
                "name": "节点1-10-4",
                "value": "1-10-4"
            }, {
                "name": "节点1-10-5",
                "value": "1-10-5",
                "list": [{
                    "name": "节点1-10-5-1",
                    "value": "1-10-5-1"
                }, {
                    "name": "节点1-10-5-2",
                    "value": "1-10-5-2"
                }, {
                    "name": "节点1-10-5-3",
                    "value": "1-10-5-3"
                }]
            }]
        }]
    }]
    data['nodesJson'] = [{"c1": "#00c7ef", "c2": "#0AF3FF"}, {"c1": "#FF8E14", "c2": "#FFA12F"},
                         {"c1": "#AF5AFF", "c2": "#B62AFF"}, {"c1": "#25dd59", "c2": "#29f463"},
                         {"c1": "#6E35FF", "c2": "#6E67FF"}, {"c1": "#002AFF", "c2": "#0048FF"},
                         {"c1": "#8CD282", "c2": "#95F300"}, {"c1": "#3B0EFF", "c2": "#604BFF"},
                         {"c1": "#00BE74", "c2": "#04FDB8"}, {"c1": "#4a3ac6", "c2": "#604BFF"}]

    data['name'] = '知识图谱'
    return json.dumps(data, ensure_ascii=False)


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
