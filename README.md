# 医学联盟网站

#### 介绍
医学联盟网站

#### 软件架构
#### 安装教程(从零搭建医学联盟网站)
##### nginx启动静态页面
###### 安装nginx
1.  yum安装nginx
```
    yum -y install nginx
    systemctl start nginx
```
2.  设置防火墙
```
    systemctl start firewalld
    firewall-cmd --permanent --zone=public --add-service=http
    firewall-cmd --permanent --zone=public --add-service=https
    firewall-cmd --zone=public --add-port=80/tcp --permanent
    firewall-cmd --reload
```
3.  服务器安全组放行80端口

4.  测试（浏览器里键入你的主机地址）

    ![nginx.png](images/nginx.png)
###### nginx启动页面
1.  修改配置文件
```
    vim /etc/nginx/nginx.conf
```
   找到root /usr/share/nginx/html;并将其修改为root 自己html路径;
```
server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        
        root         /usr/share/nginx/html;        #修改为root         /data/www;
        
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }
        
        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
```
2.  重启nginx
```
    nginx -s reload
```
3. 显示网站（模版推荐http://www.mobanwang.com/）
  ![网站首页.png](images/网站首页.png)
  
  
  
#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx


