function handle2(arr, idx, color, category) {
    arr.forEach((item, index) => {
        if (item.name === null) {
            return false;
        }
        // 设置节点大小
        let symbolSize = 10;
        switch (idx) {
            case 0:
                symbolSize = 70;
                break;
            case 1:
                symbolSize = 50;
                break;
            default:
                symbolSize = 10;
                break;
        }

        // 每个节点所对应的文本标签的样式。
        let label = null;
        switch (idx) {
            case 0:
            case 1:
                label = {
                    position: "inside",
                    rotate: 0
                };
                break;
            default:
                break;
        }

        //计算出颜色,从第二级开始
        if (idx === 0) {
            color = colors[0];
        }
        if (idx == 1) {
            color = colors.find((itemm, eq) => eq == index % 10);
            legend.push(item.name);
        }
        // 设置线条颜色
        let lineStyle = {
            color: color.c2
        };
        // 设置节点样式
        let bgcolor = null;
        if (idx === 0) {
            bgcolor = {
                type: "radial",
                x: 0.5,
                y: 0.5,
                r: 0.5,
                colorStops: [{
                        offset: 0,
                        color: color.c1 // 0% 处的颜色
                    },
                    {
                        offset: 0.8,
                        color: color.c1 // 80% 处的颜色
                    },
                    {
                        offset: 1,
                        color: "rgba(0, 0, 0, 0.3)" // 100% 处的颜色
                    }
                ],
                global: false
            };
        } else {
            bgcolor = {
                type: "radial",
                x: 0.5,
                y: 0.5,
                r: 0.5,
                colorStops: [{
                        offset: 0,
                        color: color.c1 // 0% 处的颜色
                    },
                    {
                        offset: 0.4,
                        color: color.c1 // 0% 处的颜色
                    },
                    {
                        offset: 1,
                        color: color.c2 // 100% 处的颜色
                    }
                ],
                global: false
            };
        }
        let itemStyle = null;
        if (item.list && item.list.length !== 0) {
            //非子节点
            itemStyle = {
                borderColor: color.c2,
                color: bgcolor
            };
        } else {
            //子节点
            item.isEnd = true;
            if (item.isdisease == "true") {
                itemStyle = {
                    color: color.c2,
                    borderColor: color.c2
                };
            } else {
                itemStyle = {
                    color: "transparent",
                    borderColor: color.c2
                };
            }
        }
        //可以改变来实现节点发光效果，但体验不好
        itemStyle = Object.assign(itemStyle, {
            shadowColor: "rgba(255, 255, 255, 0.5)",
            shadowBlur: 10
        });

        if (idx == 1) {
            category = item.name;
        }
        let obj = {
            name: item.name,
            symbolSize: symbolSize,
            category: category,
            label,
            color: bgcolor,
            itemStyle,
            lineStyle
        };
        obj = Object.assign(item, obj);
        if (idx === 0) {
            obj = Object.assign(obj, {
                root: true
            });
        }
        if (item.list && item.list.length === 0) {
            obj = Object.assign(obj, {
                isEnd: true
            });
        }
        list.push(obj);
        if (item.list && item.list.length > 0) {
            handle2(item.list, idx + 1, color, category);
        }
    });
}
// 计算links
function handle3(arr, index, color) {
    arr.forEach(item => {
        if (item.list) {
            item.list.forEach((item2, eq) => {
                if (index === 0) {
                    color = colors.find((itemm, eq2) => eq2 == eq % 10);
                }
                let lineStyle = null;
                switch (index) {
                    case 0:
                        if (item2.list.length > 0) {
                            lineStyle = {
                                normal: {
                                    color: "target"
                                }
                            };
                        } else {
                            lineStyle = {
                                normal: {
                                    color: color.c2
                                }
                            };
                        }
                        break;
                    default:
                        lineStyle = {
                            normal: {
                                color: "source"
                            }
                        };
                        break;
                }
                let obj = {
                    source: item.name,
                    target: item2.name,
                    lineStyle
                };
                links.push(obj);
                if (item2.list && item.list.length > 0) {
                    handle3(item.list, index + 1);
                }
            });
        }

    });
}